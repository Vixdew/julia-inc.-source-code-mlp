# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* An overview of Julia's functionality through example code 
* Made for UFRGRS' Programming Language Models class.

### How do I get set up? ###

* Install Jupyter Notebooks
* Open the MLP Demo file via the Jupyter file browser
* If you'd like to run the cells yourself, install Julia 1.4.2 or newer, then install the package "IJulia" through the Julia terminal.
